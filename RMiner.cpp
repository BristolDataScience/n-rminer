/*=================================================================================================
 * N-RMiner version 1.0 - Software to mine interesting Maximal Complete Connected Subsets (N-MCCSs)
 * from multi-relational data containing N-ary relationships.
 *
 * Copyright (C) 2011 Eirini Spyropoulou, Tijl De Bie
 * Department of Engineering Mathematics, University of Bristol, Bristol, UK
 * ------------------------------------------------------------------------------------------------
 *
 * N-RMiner-1.1 licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0)
 *================================================================================================*/

/** RMiner.cpp
 *
 *  Author: Eirini Spyropoulou
 *  email: eirini.spyropoulou@gmail.com
 **/

#include "RMiner.h"

using namespace std;

RMiner::RMiner(vector<vector<unsigned int> >* en, vector<vector<unsigned int> >* rt, vector<vector<vector<unsigned int> > >* el, vector<vector<set<unsigned int> > >* nl, vector<unsigned int>* nt, vector<vector<unsigned int> >* tr, vector<unsigned int >* ni, vector<string >* nn, map<unsigned int, NullModel3Ary*> m3, map<unsigned int, NullModel*> m2, vector<unsigned int>* cons, double density, unsigned int noe, vector<ntype>* ot, bool ci){

	RelInstIdToEntities=en;                          //linking relationship instance ids to entities ids
	RelsToTypes=rt;                                  //linking relationship types to the participating entity types
	RelInstList=el;                                 //relationship instance ids for every entity and relationship type
	EntityAugList=nl;                               //for every entity, it stores the valid augmentation elements (or the entities it is related to) in a different set for every entity type
	EntityToType=nt;                                //linking every entity to its type
	EntityToModelIndex=ni;                          //linking entity ids to their index in the MaxEnt model of the data
	EntityTypeToRelTypes=tr;                        //linking every entity type to the relationship types it participates
	EntityIdToEntityName=nn;                       //linking entity ids to the actual string they correspond to ex: film title, director name etc

	Models3=m3;                                  //MaxEnt model for 3-ary relationships
	Models2=m2;                                  //MaxEnt model for binary relationships

	constraints = cons;
	numOfEntities = noe;
	p=density;                                  //the parameter p in the description length equals the density of the data

	orderOfTypes=ot;
    
    compute_interestingness=ci;

	maxDepth=0;
	sizeOfMaxNMCCS=0;
	maxSpace=0;
	numOfNMCCSs=0;

}

int RMiner::getNumOfNMCCSs(){
	return numOfNMCCSs;
}
int RMiner::getSizeOfMaxNMCCS(){
	return sizeOfMaxNMCCS;
}
int RMiner::getMaxDepth(){
	return maxDepth;
}

int RMiner::getMaxSpace(){
	return maxSpace;
}

//calculates the size of the intersection of 2 sets
int RMiner::intersection_size(set<unsigned int>* s1, set<unsigned int>* s2){

	vector<unsigned int> res(int(s1->size()));
	vector<unsigned int>::iterator it;

	it=set_intersection(s1->begin(), s1->end(), s2->begin(), s2->end(), res.begin());
	return int(it-res.begin());
}

//
bool RMiner::new_type_introduced(vector<bool>* Rels, vector<vector<unsigned int> > rel_insts){

	for (unsigned int i=0; i<Rels->size(); i++){
		if (!((*Rels)[i]) && rel_insts[i].size()!=0){
			return true;
		}
	}

	return false;
}

//checks whether the constraints are satisfied on the set Comp\(B U C) for every etnity type (constraint upper bound)
bool RMiner::eval_constraints(vector<set<unsigned int> >* Comp, vector<set<unsigned int> >* C, set<unsigned int>* B){

	set<unsigned int>::const_iterator cit;
	unsigned int card=0;

	for (unsigned int i=0; i<Comp->size(); i++){
		card=0;
		for (cit=(*Comp)[i].begin(); cit!=(*Comp)[i].end(); ++cit){
			if((*C)[i].find(*cit)==(*C)[i].end()){
				if (B->find(*cit)==B->end())
					card++;
			}
			else {
				card++;
			}
			if (card >= (*constraints)[i])
				break;
		}
		if (card < (*constraints)[i])
			return false;
	}
	return true;
}

//computes part of the self information of the NMCCS C based on the binary relationship rel_type
double RMiner::self_info_binary_rel(vector<set<unsigned int> >* C, unsigned int rel_type){
	double si=0.0;
	set<unsigned int>::const_iterator sit1;
	set<unsigned int>::const_iterator sit2;

	vector<unsigned int> entity_types = (*RelsToTypes)[rel_type];

	for (sit1=(*C)[entity_types[0]].begin(); sit1!=(*C)[entity_types[0]].end(); ++sit1){
		for (sit2=(*C)[entity_types[1]].begin(); sit2!=(*C)[entity_types[1]].end(); ++sit2){
			si-=Models2[rel_type]->logProbability((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
		}
	}
	return si;
}

//computes part of the self information of the NMCCS C based on the 3ary relationship rel_type
double RMiner::self_info_3ary_rel(vector<set<unsigned int> >* C, unsigned int rel_type){
	double si=0.0;
	set<unsigned int>::const_iterator sit1;
	set<unsigned int>::const_iterator sit2;
	set<unsigned int>::const_iterator sit3;

	vector<unsigned int> entity_types = (*RelsToTypes)[rel_type];

	for (unsigned int i=0; i<entity_types.size(); i++){
		//the cases when one of the entity types of the 3-ary relationship is not present in the pattern
		if ((*C)[entity_types[0]].empty()){
			for (sit1=(*C)[entity_types[1]].begin(); sit1!=(*C)[entity_types[1]].end(); ++sit1){
				for (sit2=(*C)[entity_types[2]].begin(); sit2!=(*C)[entity_types[2]].end(); ++sit2){
					si-=Models3[rel_type]->logProbabilityColsTubs((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
				}
			}
		}
		else if ((*C)[entity_types[1]].empty()){
			for (sit1=(*C)[entity_types[0]].begin(); sit1!=(*C)[entity_types[0]].end(); ++sit1){
				for (sit2=(*C)[entity_types[2]].begin(); sit2!=(*C)[entity_types[2]].end(); ++sit2){
					si-=Models3[rel_type]->logProbabilityRowsTubs((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
				}
			}
		}
		else if ((*C)[entity_types[2]].empty()){
			for (sit1=(*C)[entity_types[0]].begin(); sit1!=(*C)[entity_types[0]].end(); ++sit1){
				for (sit2=(*C)[entity_types[1]].begin(); sit2!=(*C)[entity_types[1]].end(); ++sit2){
					si-=Models3[rel_type]->logProbabilityRowsCols((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
				}
			}
		}
		//the case when all entity types are present in the pattern
		else {
			for (sit1=(*C)[entity_types[0]].begin(); sit1!=(*C)[entity_types[0]].end(); ++sit1){
				for (sit2=(*C)[entity_types[1]].begin(); sit2!=(*C)[entity_types[1]].end(); ++sit2){
					for (sit3=(*C)[entity_types[2]].begin(); sit3!=(*C)[entity_types[2]].end(); ++sit3){
						si-=Models3[rel_type]->logProbability((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2],(*EntityToModelIndex)[*sit3]);
					}
				}
			}
		}
	}
	return si;
}
//computes the self information of the NMCCS C as a sum over the self information for every relationship present in the pattern
//(remember the MaxEnt model is a product of independent distributions one for every relationship type)
double RMiner::self_info(vector<set<unsigned int> >* C){
	double si = 0.0;
	int types_involved = 0;

	for (unsigned int i=0; i<(*RelsToTypes).size(); i++){
		types_involved = 0;
		for (unsigned int j=0; j<(*RelsToTypes)[i].size(); j++){
			if (!(*C)[(*RelsToTypes)[i][j]].empty()){
				types_involved++;
			}
		}
		if (types_involved>=2){
			if ((*RelsToTypes)[i].size()==2){
				si+=self_info_binary_rel(C,i);
			}
			else {
				si+=self_info_3ary_rel(C,i);
			}
		}

	}

	return si;
}

//computes part of the self information of the NMCCS C based on the binary relationship rel_type, in the case the iterative output of patterns is used,
//which at every iteration takes into account only the self information of relationship instances not already conveyed to the user.
double RMiner::self_info_binary_rel(vector<set<unsigned int> > C, unsigned int rel_type, vector<set<unsigned int> >* Conveyed){
	double si=0.0;
	set<unsigned int>::const_iterator sit1;
	set<unsigned int>::const_iterator sit2;

	vector<unsigned int> entity_types = (*RelsToTypes)[rel_type];

	for (sit1=(C)[entity_types[0]].begin(); sit1!=(C)[entity_types[0]].end(); ++sit1){
		for (sit2=(C)[entity_types[1]].begin(); sit2!=(C)[entity_types[1]].end(); ++sit2){
			if (!((*Conveyed)[entity_types[0]].find(*sit1)!=(*Conveyed)[entity_types[0]].end() && (*Conveyed)[entity_types[1]].find(*sit2)!=(*Conveyed)[entity_types[1]].end()))
				si-=Models2[rel_type]->logProbability((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
		}
	}
	return si;
}

//computes part of the self information of the NMCCS C based on the 3-ary relationship rel_type in the case the iterative output of patterns is used,
//which at every iteration takes into account only the self information of relationship instances not already conveyed to the user.
double RMiner::self_info_3ary_rel(vector<set<unsigned int> > C, unsigned int rel_type, vector<set<unsigned int> >* Conveyed){
	double si=0.0;
	set<unsigned int>::const_iterator sit1;
	set<unsigned int>::const_iterator sit2;
	set<unsigned int>::const_iterator sit3;

	vector<unsigned int> entity_types = (*RelsToTypes)[rel_type];

	for (unsigned int i=0; i<entity_types.size(); i++){
		//the cases when one of the entity types of the 3-ary relationship is not present in the pattern
		if ((C)[entity_types[0]].empty()){
			for (sit1=(C)[entity_types[1]].begin(); sit1!=(C)[entity_types[1]].end(); ++sit1){
				for (sit2=(C)[entity_types[2]].begin(); sit2!=(C)[entity_types[2]].end(); ++sit2){
					if (!((*Conveyed)[entity_types[1]].find(*sit1)!=(*Conveyed)[entity_types[1]].end() && (*Conveyed)[entity_types[2]].find(*sit2)!=(*Conveyed)[entity_types[2]].end()))
						si-=Models3[rel_type]->logProbabilityColsTubs((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
				}
			}
		}
		else if ((C)[entity_types[1]].empty()){
			for (sit1=(C)[entity_types[0]].begin(); sit1!=(C)[entity_types[0]].end(); ++sit1){
				for (sit2=(C)[entity_types[2]].begin(); sit2!=(C)[entity_types[2]].end(); ++sit2){
					if (!((*Conveyed)[entity_types[0]].find(*sit1)!=(*Conveyed)[entity_types[0]].end() && (*Conveyed)[entity_types[2]].find(*sit2)!=(*Conveyed)[entity_types[2]].end()))
						si-=Models3[rel_type]->logProbabilityRowsTubs((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
				}
			}
		}
		else if ((C)[entity_types[2]].empty()){
			for (sit1=(C)[entity_types[0]].begin(); sit1!=(C)[entity_types[0]].end(); ++sit1){
				for (sit2=(C)[entity_types[1]].begin(); sit2!=(C)[entity_types[1]].end(); ++sit2){
					if (!((*Conveyed)[entity_types[0]].find(*sit1)!=(*Conveyed)[entity_types[0]].end() && (*Conveyed)[entity_types[1]].find(*sit2)!=(*Conveyed)[entity_types[1]].end()))
						si-=Models3[rel_type]->logProbabilityRowsCols((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2]);
				}
			}
		}
		else {
			//the case when all entity types are present in the pattern
			for (sit1=(C)[entity_types[0]].begin(); sit1!=(C)[entity_types[0]].end(); ++sit1){
				for (sit2=(C)[entity_types[1]].begin(); sit2!=(C)[entity_types[1]].end(); ++sit2){
					for (sit3=(C)[entity_types[2]].begin(); sit3!=(C)[entity_types[2]].end(); ++sit3){
						if (!((*Conveyed)[entity_types[0]].find(*sit1)!=(*Conveyed)[entity_types[0]].end() && (*Conveyed)[entity_types[1]].find(*sit2)!=(*Conveyed)[entity_types[1]].end() && (*Conveyed)[entity_types[2]].find(*sit3)!=(*Conveyed)[entity_types[2]].end()))
							si-=Models3[rel_type]->logProbability((*EntityToModelIndex)[*sit1],(*EntityToModelIndex)[*sit2],(*EntityToModelIndex)[*sit3]);
					}
				}
			}
		}
	}
	return si;
}

//computes the self information of the NMCCS C in the case the iterative output of patterns is used,
//which at every iteration takes into account only the self information of relationship instances not already conveyed to the user.
//(remember the MaxEnt model is a product of independent distributions one for every relationship type)
double RMiner::self_info(vector<set<unsigned int> > C, vector<set<unsigned int> >* Conveyed){
	double si = 0.0;
	int types_involved = 0;

	for (unsigned int i=0; i<(*RelsToTypes).size(); i++){
		types_involved = 0;
		for (unsigned int j=0; j<(*RelsToTypes)[i].size(); j++){
			if (!(C)[(*RelsToTypes)[i][j]].empty()){
				types_involved++;
			}
		}
		if (types_involved>=2){
			if ((*RelsToTypes)[i].size()==2){
				si+=self_info_binary_rel(C,i,Conveyed);
			}
			else {
				si+=self_info_3ary_rel(C,i,Conveyed);
			}
		}

	}

	return si;
}

//computes the description length of the pattern C using the global variable p as a parameter
double RMiner::desc_length(vector<set<unsigned int> >* C){
	vector<set<unsigned int> >::const_iterator sit;
	int entitiesIn=0;
	double res;

	for (sit=C->begin(); sit!=C->end(); sit++){
		entitiesIn+=sit->size();
	}
	res = -entitiesIn*log(p)-(numOfEntities-entitiesIn)*log(1-p);
	return res;
}

//from the set of all NMCCSs it iteratively prints k, each time considering
//the sel-information of the relationship instances not already presented to the user
//when k=0 it simply sorts and prints the whole list of patterns.
void RMiner::iteratively_print(int k, fstream& outfile){
	int i=0;
	
	set<unsigned int>::const_iterator sit;
	vector<inode>::iterator lit, eit;
    set<unsigned int> set_int;
	vector<set<unsigned int> > entitiesAlreadyConveyed;

    if (k!=0){
        while (i<k && i<NMCCSsList.size()){
            i++;
            i_sort sortingfunction;
            sort(InfoList.begin(), InfoList.end(), sortingfunction);
            int j = InfoList[0].patternIndex;
            outfile<<InfoList[0].interestingness<<" ";
        
            for (unsigned int m=0; m<NMCCSsList[j].size(); m++){
                entitiesAlreadyConveyed.push_back(set_int);
                for (sit=NMCCSsList[j][m].begin(); sit!=NMCCSsList[j][m].end(); ++sit){
                    outfile<<(*EntityIdToEntityName)[*sit]<<" ";
                    entitiesAlreadyConveyed[m].insert(*sit);
                }
            }
        
            outfile<<endl;
            InfoList.erase(InfoList.begin());
            for (unsigned int m=0; m<InfoList.size(); m++){
                int c = InfoList[m].patternIndex;
                InfoList[m].interestingness=self_info(NMCCSsList[c],&entitiesAlreadyConveyed)/(InfoList[m].desc_length);
            }
        }
    }
    else {
        if (compute_interestingness){
            i_sort sortingfunction;
            sort(InfoList.begin(), InfoList.end(), sortingfunction);
            for (unsigned int j=0; j<NMCCSsList.size(); j++){
                int k=InfoList[j].patternIndex;
                outfile<<InfoList[j].interestingness<<" ";
                for (unsigned int m=0; m<NMCCSsList[j].size(); m++){
                    for (sit=NMCCSsList[k][m].begin(); sit!=NMCCSsList[k][m].end(); ++sit){
                        outfile<<(*EntityIdToEntityName)[*sit]<<" ";
                    }
                }
                outfile<<endl;
            }
        }
        else {
            for (unsigned int j=0; j<NMCCSsList.size(); j++){
                for (unsigned int m=0; m<NMCCSsList[j].size(); m++){
                    for (sit=NMCCSsList[j][m].begin(); sit!=NMCCSsList[j][m].end(); ++sit){
                        outfile<<(*EntityIdToEntityName)[*sit]<<" ";
                    }
                }
                outfile<<endl;
            }
        }
    }
}

//returns true if the entity participates in at least one relationship instance for every relationship type
//that the entity type participates in, false otherwise (due to errors in the data)
bool RMiner::connected_to_all_rel_types(unsigned int entityType, unsigned int entity){

	for (unsigned int i=0; i<(*EntityTypeToRelTypes)[entityType].size(); i++){
		if ((*RelInstList)[entity][(*EntityTypeToRelTypes)[entityType][i]].empty())
			return false;
	}
	return true;
}

//evaluate the constraints on the augmentation elements of the entity
bool RMiner::eval_early_constraints(unsigned int entity){

	for (unsigned int i=0; i<(*EntityAugList)[entity].size(); i++){
        
		if ((*EntityAugList)[entity][i].size()<(*constraints)[i] && (*EntityAugList)[entity][i].size()!=0)
			return false;
	}
	return true;
}

//
bool RMiner::is_NMCCS(vector<set<unsigned int> >* C, vector<set<unsigned int> >* Comp, vector<bool>* activeEntityTypes, unsigned int level){
	
	set<unsigned int>::const_iterator sit;
	int sizeOfComp = 0;

    for(unsigned int i=0; i<Comp->size(); i++){
		if ((*activeEntityTypes)[i] || level==0){
			for (sit=(*Comp)[i].begin(); sit!=(*Comp)[i].end(); ++sit){
				if ((*C)[i].find(*sit)==(*C)[i].end()){
					sizeOfComp++;
				}
			}
		}
	}
	if (sizeOfComp==0)
		return true;
	else
		return false;
}

//checking compatibility of an element with part of a set corresponding to one relationship type
//iteratively constructs all critical sets and checks for compatibility
//rel_type is the relationship type
//entityTypes the entity types involved in this relationship type
//s the set
//s_rel_insts intersection of the relationship instances of the relationship rel_type of the entities in the critical set
bool RMiner::is_comp_with_one_rel(unsigned int rel_type, vector<unsigned int>* s_rel_insts, vector<set<unsigned int> >* s, set<unsigned int> entityTypes){

	set<unsigned int>::const_iterator tit;
	set<unsigned int>::const_iterator sit;
	bool ok = true;

	if (s_rel_insts->empty())          //checks compatibility by checking whether the intersection of relationship instances is non-empty
		return false;

	for (tit=entityTypes.begin(); tit!=entityTypes.end(); ++tit){ //expands the current critical set
		for (sit=(*s)[*tit].begin(); sit!=(*s)[*tit].end(); ++sit){
			vector<unsigned int> new_s_rel_insts;
			if (s_rel_insts->size()<(*RelInstList)[*sit][rel_type].size())
				new_s_rel_insts.reserve(s_rel_insts->size());
			else
				new_s_rel_insts.reserve((*RelInstList)[*sit][rel_type].size());
            
			insert_iterator<vector<unsigned int> > it = insert_iterator<vector<unsigned int> >(new_s_rel_insts, new_s_rel_insts.begin());
			set_intersection(s_rel_insts->begin(), s_rel_insts->end(), (*RelInstList)[*sit][rel_type].begin(), (*RelInstList)[*sit][rel_type].end(), it); //computes intersection of the new set
			set<unsigned int> new_entityTypes=entityTypes;
			new_entityTypes.erase(*tit); //at the next recursion only consider different entity types (this is how critical sets are built)
			ok = is_comp_with_one_rel(rel_type, &new_s_rel_insts, s, new_entityTypes);
			if (!ok)
				return false;
		}
	}
	return ok;
}

//checks compatibility of an element with a set
bool RMiner::is_comp(int entity, vector<set<unsigned int> >* s){

	
	set<unsigned int>::const_iterator sit;
	set<unsigned int>::const_iterator nit;

	unsigned int num_of_rel_insts=1;
	bool empty_rel=true;

	if (!connected_to_all_rel_types((*EntityToType)[entity], entity)){
		return false;
	}
	else {
        for (unsigned int j=0; j<(*RelInstList)[entity].size(); j++){
    
			set<unsigned int> entityTypes;
			num_of_rel_insts=1;
			empty_rel=true;

			for (unsigned int i=0; i<(*RelsToTypes)[j].size(); i++){
				if ((*RelsToTypes)[j][i]!=(*EntityToType)[entity]){
					entityTypes.insert((*RelsToTypes)[j][i]);     //construct vector of all entity types of the relationship type except the type of the entity
					if (!(*s)[(*RelsToTypes)[j][i]].empty()){     //calculates the number of critical sets that should be covered for the current relationship type
						num_of_rel_insts*=(*s)[(*RelsToTypes)[j][i]].size();
						empty_rel = false;
					}
				}
			}

			if (empty_rel)
				num_of_rel_insts=0;

			if (!(*RelInstList)[entity][j].empty()){                           //if empty it means that this entity type is not part of this relationship type
				if ((*RelInstList)[entity][j].size()>=num_of_rel_insts){       //actually only checks compatibility if the number of relationship instances that the entity participates is greater than the number of critical sets that should be covered
					vector<unsigned int> rel_inst_list = (*RelInstList)[entity][j];
					if (!is_comp_with_one_rel(j, &rel_inst_list, s, entityTypes))
							return false;
				}
				else {
					return false;
				}
			}

		}
	}

	return true;
}

//the actual algorithm C: an N-CCS, B:the set B as in the pseudocode, Comp:the set of compatible elements,
//activeRelTypes:boolean vector over all relationship types containing 1 if there is at least one entity in Cfrom an entity type participating in the relationship type
//activeEntityTypes:boolean vector over all entity types containing 1 if the entity type is related to at least one entity type already in the solution, 0 otherwise
//level: the depth of the search tree
//consat: boolean value specifying wheher the constraints have been met
void RMiner::run(vector<set<unsigned int> >* C, set<unsigned int>* B, vector<set<unsigned int> >* Comp, vector<bool>* activeRelTypes, vector<bool>* activeEntityTypes, unsigned int level, bool consat){

	set<unsigned int>::const_iterator sit;
	set<unsigned int>::const_iterator cit;
	set<unsigned int>::const_iterator nit;

//*************************************************************
/** lines to get the current usage of space (only for linux!!!)**/

/*	struct rusage usage;
		getrusage(RUSAGE_SELF, &usage);
		if (maxSpace<usage.ru_maxrss)
				maxSpace=usage.ru_maxrss;
*/
//************************************************************
    
	if(is_NMCCS(C,Comp,activeEntityTypes,level)){  //if C is an NMCCS

		unsigned int pattern_size=0;
		for (unsigned int i=0; i<C->size(); i++){
			pattern_size+=(*C)[i].size();
		}

		NMCCSsList.push_back(*C);
        if (compute_interestingness){
            inode n;
            n.patternIndex = numOfNMCCSs;
            n.desc_length = desc_length(C);
            n.interestingness = self_info(C)/desc_length(C);
            InfoList.push_back(n);
        }

		if (pattern_size>sizeOfMaxNMCCS)
			sizeOfMaxNMCCS=pattern_size;

		numOfNMCCSs++;

	}
	else {

		//decide the order in which the entities are going to be considered
		//the order depends only on the entity type and is decided based on
		//the following rule (function p_sort): if there is an entity type
		//with an associated constraint that is not yet satisfied then entities
		//of this type are considered first. Otherwise the ordering of types depends
		//on their average connectivity (i.e. average number of related relationship instances)
		//entity types with lower av. connectivity are considered first.

		vector<ntype> new_orderOfTypes = (*orderOfTypes);
		for (unsigned int t=0; t<orderOfTypes->size(); t++){
			unsigned int nt = (*orderOfTypes)[t].type;
			if ((*C)[nt].size()<(*constraints)[nt])
				new_orderOfTypes[t].priority=true;
		}
		p_sort sortingfunction;
		sort(new_orderOfTypes.begin(), new_orderOfTypes.end(), sortingfunction);

		set<unsigned int> newB = (*B);
		for (unsigned int t=0; t<new_orderOfTypes.size(); t++){
			if ((*activeEntityTypes)[new_orderOfTypes[t].type] || level==0){    //using the active node types ensures that we always pick elements from the Aug set.
			unsigned int nt = new_orderOfTypes[t].type;

			for (nit=(*Comp)[nt].begin(); nit!=(*Comp)[nt].end(); ++nit){
				if ((*C)[nt].find(*nit)==(*C)[nt].end()){
					if (B->find(*nit)==B->end()){
						newB.insert(*nit);
                        
						if (connected_to_all_rel_types(nt, *nit)){                           //if there is at least one relationship instance for every relationship type that the entity type participates in
                         
						if (eval_early_constraints(*nit)){									//since for all types i in Aug(C) it holds that Aug_i(C)=Comp_i(C) and Aug(C union e)=Aug(C) intersection Aug(e), if the constraints are not satisfied in Aug(e) the upperbound of the constraint is going to be false

							vector<set<unsigned int> > closed_newC = (*C);         //initialise the new set with the elements already in C

							closed_newC[nt].insert(*nit);                                    //add the current element

							vector<bool> new_activeRelTypes = (*activeRelTypes);
							vector<bool> new_activeEntityTypes = (*activeEntityTypes);

							for (unsigned int k=0; k<(*RelInstList)[*nit].size(); k++){     //for all rel types of the relationship instance list
								if ((*RelInstList)[*nit][k].size()>0){
									new_activeRelTypes[k]=true;
									vector<unsigned int> types = (*RelsToTypes)[k];					//find which entity types are involved in this rel type
									for (unsigned int j=0; j<types.size(); j++){
										if (types[j]!=(*EntityToType)[*nit])
											new_activeEntityTypes[types[j]]=true;                               //update the active entity types
									}
								}
							}

							//update the set of compatible elements
							vector<set<unsigned int> > new_Comp;
                            for (unsigned int s=0; s<new_orderOfTypes.size(); s++){
                                set<unsigned int> set_int;
                                new_Comp.push_back(set_int);
                            }
                            
							int count=0;

							for (unsigned int s=0; s<new_orderOfTypes.size(); s++){                    //the members of the C are just directly inserted
								unsigned int tt = new_orderOfTypes[s].type;
								if (new_activeEntityTypes[tt]){
									if (!(*activeEntityTypes)[tt]){                                    //entity type has just become active
										new_Comp[tt]=(*EntityAugList)[*nit][tt];
									}
									else {
										set<unsigned int> temp;
										new_Comp[tt]=temp;
										for (cit=(*Comp)[tt].begin(); cit!=(*Comp)[tt].end(); ++cit){
											if (closed_newC[tt].find(*cit)!=closed_newC[tt].end())
												new_Comp[tt].insert(*cit);
											else {
												bool a=is_comp(*cit, &closed_newC);
												if (a){
													count++;
													new_Comp[tt].insert(*cit);
												}
											}
										}
									}
								}
								else {
									new_Comp[tt]=(*Comp)[tt];
								}
							}

							bool potential_consat;                                                              //true if the constraints are already satidfied or if the upper bound is true
							if (consat){
								potential_consat=true;
							}
							else {
								potential_consat=eval_constraints(&new_Comp, &closed_newC, &newB);
							}

							if (potential_consat){                                           //only compute closure if the upper
                                                                                            //bound of the constraint is satisfied and if not at depth 0.

								//**** Find closure************//////////
								bool overlap = false;
								for (unsigned int s=0; s<new_orderOfTypes.size(); s++){
									unsigned int tt = new_orderOfTypes[s].type;
									if (new_activeEntityTypes[tt]){
										for (sit=new_Comp[tt].begin(); sit!=new_Comp[tt].end(); ++sit){
											if (closed_newC[tt].find(*sit)==closed_newC[tt].end()){
												if (!new_type_introduced(&new_activeRelTypes, (*RelInstList)[*sit])){
													bool a =is_comp(*sit, &new_Comp);
													if (a){
														if (newB.find(*sit)!=newB.end()){
															overlap=true;
															break;
														}
														else {
															closed_newC[tt].insert(*sit);

														}
													}
												}
											}
										}
									}
								}

								bool newconsat;

								if (consat) {
									newconsat=true;
								}
								else {
									newconsat=true;
									for (unsigned int d=0; d<closed_newC.size(); d++){
										if (closed_newC[d].size()<(*constraints)[d])
											newconsat=false;
									}
								}


								if (!overlap){ //closure has no overlap with B
									set<unsigned int> closed_newB = newB;
									unsigned int new_level=level+1;
									if (new_level>maxDepth)
										maxDepth=level;
									run(&closed_newC, &closed_newB, &new_Comp, &new_activeRelTypes, &new_activeEntityTypes, new_level, newconsat);
									closed_newB.clear();
								}
							}
							new_activeRelTypes.clear();
							new_activeEntityTypes.clear();
							closed_newC.clear();
							new_Comp.clear();
						}
						}
					}
				}
			}
		}
		}
	}
}



