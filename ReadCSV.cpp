/*=================================================================================================
 * N-RMiner version 1.0 - Software to mine interesting Maximal Complete Connected Subsets (N-MCCSs)
 * from multi-relational data containing N-ary relationships.
 *
 * Copyright (C) 2011 Eirini Spyropoulou, Tijl De Bie
 * Department of Engineering Mathematics, University of Bristol, Bristol, UK
 * ------------------------------------------------------------------------------------------------
 *
 * N-RMiner-1.1 licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0)
 *================================================================================================*/

/** DatabaseGenerator.cpp
 *
 *  Author: Eirini Spyropoulou
 *  email: eirini.spyropoulou@gmail.com
 **/

#include <dirent.h>
#include <iostream>
#include <errno.h>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <cstring>
#include "RMiner.h"

using namespace std;

//comparison operator used to sort all the entities in terms of increasing number of the relationship instances
//they participate in, or in other words their connectivity.
struct bsort {
  bool operator()(cnode i,cnode j) {
	  if (i.connectivity<j.connectivity)
		  return true;
	  else
		  return false;
  }
}my_sort;

int main(int argc, char **argv) {

	string line;
	string type;
    string field;
	int argnumber=0;


	string outfile;
	vector<string> OnetoOne;
	vector<string> Nary;
	vector<unsigned int> constraints;
	vector<unsigned int> read_constraints;

	DIR *dp;
	struct dirent *dirp;

	vector<vector<unsigned int> > RelInstIdToEntities; 			  //linking relationship instance ids to entities ids
	vector<vector<unsigned int> > RelsToTypes;					  //linking relationship types to the participating entity types
	vector<vector<vector<unsigned int> > > RelInstList;           //relationship instance ids for every entity and relationship type
	vector<vector<set<unsigned int> > > EntityAugList;            //for every entity, it stores the valid augmentation elements (or the entities it
                                                                  //is related to) in a different set for every entity type
    
	vector<map<unsigned int,unsigned int > > EntityIDToUniqueID;  //structure used to give ids to entities in terms of increasing number of
                                                                  //relationship instances they participate in
	vector<unsigned int> EntityToType;                            //linking every entity to its type
	vector<vector<unsigned int> > EntityTypeToRelTypes;           //linking every entity type to the relationship types it participates
	vector<unsigned int> EntityToModelIndex;                      //linking entity ids to their index in the MaxEnt model of the data
	vector<string> EntityIdToEntityName;                       //linking entity ids to the actual string they correspond to ex: film title, director name etc
	map<unsigned int, unsigned int> numOfEntitiesPerType;
	map<unsigned int, unsigned int> numOfRelInstsPerType;
	map<string, unsigned int> typeToTypeID;

	map<unsigned int, NullModel3Ary*> Models3;								//MaxEnt model for 3-ary relationships
	map<unsigned int, NullModel*> Models2;									//MaxEnt model for binary relationships

	//initialisations
	int entityID = 0;
    unsigned int numOfEntities = 0;
	unsigned int entityIndex = 0;
	unsigned int relInstIndex = 0;
	unsigned int relInstID = 0;
	unsigned int entityType = 0;
    unsigned int reltype=0;
	double numOfPossibleRelInsts = 0;

	vector<set<unsigned int> > C;							  //initial solution C
	vector<set<unsigned int> > Comp;						  //Compatible elements initialisation (i.e. all entities) (per type)
	vector<set<unsigned int> > RelInsts_C;					  //Relationship instance ids in the set C
	vector<set<unsigned int> > RelInsts_Comp;                 //Relationship instance ids in the set Comp
	vector<bool> activeRelTypes;							  //boolean vector over all relationship types containing 1 if there is at least one
                                                              //entity from an entity type participating in the relationship type
	vector<bool> activeEntityTypes;							  //boolean vector over all entity types containing 1 if the entity type is related
                                                              //to at least one entity type already in the solution, 0 otherwise

	vector<map <unsigned int, int> >::const_iterator tit;
	map <unsigned int, int>::const_iterator nit;
	vector<cnode> nodeVector;
    
    bool compute_interestingness=true;
    int num_to_print=200;

    FILE* fp;
    char file[1000];
    char lineo[1000];
    char cons[10];
	fp = fopen(argv[1], "r");
    char* p;
    char *l;
    
    if (fp==NULL){
        fprintf(stderr, "Can't read %s\n", argv[1]);
        exit(1);
    }
	while (fgets(lineo, 1000, fp)!=NULL){ //read the config file
		argnumber++;
        bool isi=false;
        bool isr=false;
        
        l=strchr(lineo, '\r');
        if (l!=NULL)
            *l='\0';
        l=strchr(lineo, '\n');
        if (l!=NULL)
            *l='\0';
        
        if (argnumber==1 || argnumber==2){
            isi=false;
            isr=false;
            
            char* f=lineo;
            while (*f!=' '){
                if (*f=='-'){
                    
                }
                else if (*f=='i')
                    isi=true;
                else if (*f=='r')
                    isr=true;
                else{
                    cout<<"Please specify the -r and -i parameters in the config file."<<endl;
                    return errno;
                }
                f++;
            }
            int i=0;
            char n[20];
            while (*f!='\0'){
                if (isi){
                    if (*f=='1')
                        compute_interestingness=true;
                    else
                        compute_interestingness=false;
                }
                else if (isr){
                    n[i]=*f;
                }
                
                f++;
                i++;
            }
            if (isr){
                n[i]='\0';
                num_to_print=atoi(n);
                if (compute_interestingness==false)
                    num_to_print=0;
            }
        }
		else if (argnumber==3){ //output file
			outfile=lineo;
        }
		else {
            
            p=strchr(lineo, ' ');
            
            if (p!=NULL){ //one_to_one file with constraint
                
                int i=0;
                char* f=lineo;
                while (f!=p){
                    file[i] = *f;
                    i++;
                    f++;
                }
                file[i]='\0';
                OnetoOne.push_back(file);
                i=0;
                f++;
                while (*f!='\0'){
                    cons[i]=*f;
                    f++;
                    i++;
                }
                cons[i]='\0';
                read_constraints.push_back(atoi(cons));
                constraints.push_back(0);
            }
            else {
                
                if((dp  = opendir(lineo)) == NULL) {
                    cout << "Error(" << errno << ") nary opening directory: " <<lineo << endl;
                    return errno;
                }

                while ((dirp = readdir(dp)) != NULL) {
                    
                    if(string(dirp->d_name)==string(".") || string(dirp->d_name)==string("..") || string(dirp->d_name)==string(".svn"))
                        continue;
                    Nary.push_back(string(lineo)+string(dirp->d_name));
                    //init
                    activeRelTypes.push_back(false);
                    set<unsigned int> tempt;
                    RelInsts_Comp.push_back(tempt);
                    RelInsts_C.push_back(tempt);
                }
                closedir(dp);
            }
        }
    }
    
    for (unsigned int i=0; i<OnetoOne.size(); i++){
        map<unsigned int,unsigned int> temp;
        set<unsigned int> temps;
        vector<unsigned int> tempv;
        EntityIDToUniqueID.push_back(temp);
        activeEntityTypes.push_back(false);
        EntityTypeToRelTypes.push_back(tempv);
		Comp.push_back(temps);
		C.push_back(temps);
    }
    
	//process the relationship files first to get the number of relationship instances for every entity and sort the entities based on it
	for (unsigned int b=0; b<Nary.size(); b++){
        
		vector<unsigned int> types;
		unsigned int i;
        
        reltype++;

		fstream narystream(Nary[b].c_str(),ios::in);
		getline(narystream, line);
		if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
					line.erase(line.length()-1);

		stringstream line_stream(line);
		while (getline(line_stream, type, ',')){
			if (typeToTypeID.find(type)==typeToTypeID.end()){
				typeToTypeID[type]=entityType;
				entityType++;
			}

			types.push_back(typeToTypeID[type]);
		}
        
		while(getline(narystream, line)){
			if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
				line.erase(line.length()-1);
			stringstream line_stream(line);
			i=0;
            
			while (getline(line_stream, field, ',')){
                
                if (EntityIDToUniqueID[types[i]].find(atoi(field.c_str()))==EntityIDToUniqueID[types[i]].end()){
                    cnode n;
                    n.type=types[i];
                    n.node=atoi(field.c_str());
                    n.connectivity=1;
                    nodeVector.push_back(n);
                    EntityIDToUniqueID[types[i]][atoi(field.c_str())]=nodeVector.size()-1;
                }
                else {
                    cnode n;
                    n.type=types[i];
                    n.node=atoi(field.c_str());
                    nodeVector[EntityIDToUniqueID[types[i]][atoi(field.c_str())]].connectivity++;
                }
				i++;
			}
		}
		types.clear();
	}

	//sort the entities based on number of rel instances
	sort(nodeVector.begin(), nodeVector.end(), my_sort);

	//change entity ids based on the sorting
	for (unsigned int i=0; i<nodeVector.size(); i++){
        //new ids for the entities
		EntityIDToUniqueID[nodeVector[i].type][nodeVector[i].node]=i;
        //initialisation
        EntityIdToEntityName.push_back("");
        EntityToModelIndex.push_back(0);
        EntityToType.push_back(nodeVector[i].type);
        vector<set<unsigned int> > e_set;
        for (unsigned int j=0; j<entityType; j++){
            set<unsigned int> s;
            e_set.push_back(s);
        }
        EntityAugList.push_back(e_set);
        vector<vector<unsigned int> > r_set;
        for (unsigned int j=0; j<reltype; j++){
            vector<unsigned int> s;
            r_set.push_back(s);
        }
        RelInstList.push_back(r_set);
        e_set.clear();
        r_set.clear();
	}
    
    nodeVector.clear();

	//read the one_to_one files and assign a string to every entity id
	for (unsigned int a=0; a<OnetoOne.size(); a++){
		entityIndex=0;
		
		fstream onestream(OnetoOne[a].c_str(),ios::in);
		getline(onestream,line);

		if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
                	line.erase(line.length()-1);

		stringstream line_stream(line);
		getline(line_stream, type, ','); //gets the id
		getline(line_stream, type, ','); //gets the type
		
		entityType=typeToTypeID[type];

		constraints[entityType]=read_constraints[a];
		
		char delim;
		string id;
		string name;
		unsigned int idn;

		while(getline(onestream,line)){
			
			if (line.find('\t')!=string::npos)
				delim='\t';
			else
				delim=',';

			if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
				line.erase(line.length()-1);

			stringstream line_stream(line);
			getline(line_stream, id, delim);
			getline(line_stream, name, delim);

			idn = atoi(id.c_str());
			if (EntityIDToUniqueID[entityType].find(idn)!=EntityIDToUniqueID[entityType].end()){
				entityID=EntityIDToUniqueID[entityType][idn];
				EntityIdToEntityName[entityID]=type+"."+name;
				EntityToModelIndex[entityID]=entityIndex;
				Comp[entityType].insert(entityID);
                numOfEntities++;

                //set<unsigned int> tempv;
				//for (unsigned int b=0; b<Nary.size(); b++){ //for all relationship types initialise the edge list for the node
					//RelInstList[entityID].push_back(tempv);
				//}

				entityIndex++;
			}

		}
		numOfRelInstsPerType[entityType]=0;
		numOfEntitiesPerType[entityType]=entityIndex;
	}

	//Read again the relationship files to initialise structures, assign rel instance ids to entities and compute the number of rel instances for every entity in order to buid the MaxEnt model
    reltype=0;
	for (unsigned int b=0; b<Nary.size(); b++){
		
		relInstIndex = 0;
		vector<vector<int> > sumsdim;           //model marginals
		vector<unsigned int> temps;

		fstream narystream(Nary[b].c_str(),ios::in);
		getline(narystream, line);
		if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
			line.erase(line.length()-1);

		stringstream line_stream(line);

		int relation_dim=0;
		double num=1;

		while (getline(line_stream, type, ',')){
			relation_dim++;
			num = num * numOfEntitiesPerType[typeToTypeID[type]];
			//associate every type with the relationship
			temps.push_back(typeToTypeID[type]);
            
			EntityTypeToRelTypes[typeToTypeID[type]].push_back(reltype);
			//initialise marginals for Null model
            
			vector<int> temp;
            
			for (unsigned int i=0; i<numOfEntitiesPerType[typeToTypeID[type]]; i++){
				temp.push_back(0);
			}
			sumsdim.push_back(temp);
			temp.clear();
		}
		RelsToTypes.push_back(temps);
		temps.clear();
		numOfPossibleRelInsts+=num;

		unsigned int i;
		unsigned int node;
		vector<unsigned int> fields;
		vector<unsigned int> newfields;
		string edge;
		while(getline(narystream, line)){
			if (line.find('\r')!=string::npos || line.find('\n')!=string::npos)
				line.erase(line.length()-1);
			stringstream line_stream(line);

			i=0;
			while (getline(line_stream, field, ',')){

				node=EntityIDToUniqueID[RelsToTypes[reltype][i]][atoi(field.c_str())];
				fields.push_back(node);
				sumsdim[i][EntityToModelIndex[node]]++;
				i++;
			}

			relInstID++;
			relInstIndex++;
			RelInstIdToEntities.push_back(fields);
			RelInsts_Comp[reltype].insert(relInstID);

			for (int t=0; t<relation_dim; t++){
				RelInstList[fields[t]][reltype].push_back(relInstID);
				for (int f=0; f<relation_dim; f++){
					if (f!=t)
						EntityAugList[fields[t]][RelsToTypes[reltype][f]].insert(fields[f]);
				}
			}

			fields.clear();
		}

		//builing the MaxEnt model
		if (relation_dim==2){
			NullModel* nm2 = new NullModel(&sumsdim[0], &sumsdim[1]);
			Models2[reltype]=nm2;
		}
		else if (relation_dim==3){
			NullModel3Ary* nm3 = new NullModel3Ary(&sumsdim[0], &sumsdim[1], &sumsdim[2]);
			Models3[reltype]=nm3;
		}
		for (unsigned int i=0; i<RelsToTypes[reltype].size(); i++){
			numOfRelInstsPerType[RelsToTypes[reltype][i]]+=relInstIndex;
		}
		reltype++;
        sumsdim.clear();
	}

	//////*******DEBUG LINES************************//////////////
	/*set<unsigned int>::const_iterator it;
	vector<set<unsigned int> >::const_iterator it3;
	vector<vector<set<unsigned int> > >::const_iterator it2;
	string debug="debug.txt";
	fstream dfile(debug.c_str(),ios::out);

	for(it2=RelInstList.begin(); it2!=RelInstList.end(); ++it2){
		dfile<<EntityIdToEntityName[(*it2).first]<<endl;
		for(it3=(*it2).second.begin(); it3!=(*it2).second.end(); ++it3){
			dfile<<"Reltype:"<<(*it3).first<<" ";
			vector<unsigned int> types;
			types=RelsToTypes[(*it3).first];
			for (it=(*it3).second.begin(); it!=(*it3).second.end(); ++it){
				vector<int> nodes;
				nodes=RelInstIdToEntities[(*it)];
				dfile<<*it<<" ";
				for (unsigned int l=0; l<nodes.size(); l++){
					dfile<<types[l]<<": "<<EntityIdToEntityName[nodes[l]]<<" ";
				}
				dfile<<endl;
			}
			dfile<<endl;
		}
	}
	dfile.close();*/

	////////*********************************/////////////////

	////Find order of node types based on average number of links

	 vector<ntype> order;

	 for (unsigned int i=0; i<OnetoOne.size(); i++){
		 ntype ct;
		 ct.type=i;
		 ct.average_degree = (double)numOfRelInstsPerType[i]/(double)numOfEntitiesPerType[i];
		 ct.priority=false;
		 order.push_back(ct);
	 }

	 p_sort sortingfunction;
	 sort(order.begin(),order.end(),sortingfunction);

	bool csat=true;
	for (unsigned int i=0; i<order.size(); i++){
		if (constraints[i]!=0)
			csat=false;
	}

	
	set<unsigned int> B;

	double density = ((double)relInstID)/numOfPossibleRelInsts; //the density of the data to be used as the parameter p in the description length
	fstream outputfile(outfile.c_str(),ios::out);
	clock_t begin = clock();
	RMiner* r = new RMiner(&RelInstIdToEntities, &RelsToTypes, &RelInstList, &EntityAugList, &EntityToType, &EntityTypeToRelTypes, &EntityToModelIndex, &EntityIdToEntityName, Models3, Models2, &constraints, density, numOfEntities, &order, compute_interestingness);
	r->run(&C, &B, &Comp, &activeRelTypes, &activeEntityTypes, 0, csat);
	clock_t end = clock();
	double time_sec = (double(end - begin) / CLOCKS_PER_SEC);
	r->iteratively_print(num_to_print,outputfile);
	outputfile.close();

	/************** Print the statistics ********************/
	cout<<"time (sec): "<<time_sec<<endl;
	cout<<"NMCCSs: "<<r->getNumOfNMCCSs()<<endl;
	cout<<"Max NMCCS: "<<r->getSizeOfMaxNMCCS()<<endl;
	cout<<"Max Depth: "<<r->getMaxDepth()<<endl;
	cout<<"Max space: "<<r->getMaxSpace()<<endl;
	/*****************************************************/

	/*************************************/

}
